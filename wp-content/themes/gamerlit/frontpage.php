<?php
/**
* Template Name: Front Page
 */

get_header(); ?>
	<section id="welcome-wrap">
		<div class="welcome">
			<div class="centered-welcome">
				<?php global $post;

					if(get_post_meta( $post->ID, 'g_announcement_title', true )) :
						$antitle = get_post_meta( $post->ID, 'g_announcement_title', true );
						$animage = get_post_meta( $post->ID, 'g_announcement_img', true );
						$andesc  = get_post_meta( $post->ID, 'g_announcement_content', true );

				?>
					<div class="g-announcement">

						<div class="g-announcement-content">
							<div class="g-announcement-title"><?php echo $antitle; ?></div>
							<div class="g-announcement-text">
								
								<?php echo $andesc; ?>

								</div>
						</div>
						<div class="g-announcement-image">
							<img src="<?php echo $animage; ?>" />
						</div>
						<div class="g-announcement-x">
							x
						</div>
					</div>

				<?php endif; ?>
				<div class="g-blocks">
					<div class="g-block">
						<div class="g-block-title">categories</div>
						<div class="g-block-image" style="background-image:url('<?php echo GAMERLIT_2X . 'dice2.png'; ?>')">
							
						</div>
					</div>
					<div class="g-block">
						<div class="g-block-title">bookstore</div>
						<div class="g-block-image" style="background-image:url('<?php echo GAMERLIT_2X . 'bookshop2.png'; ?>')">
							
						</div>
					</div>
					<div class="g-block">
						<div class="g-block-title">community</div>
						<div class="g-block-image fit-width" style="background-image:url('<?php echo GAMERLIT_2X . 'community2.png'; ?>')">
							
						</div>
					</div>
					<div class="g-block">
						<div class="g-block-title">support us</div>
						<div class="g-block-image" style="background-image:url('<?php echo GAMERLIT_2X . 'support2.png'; ?>')">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="primary" class="content-area col-sm-12">
		<main id="main" class="site-main" role="main">

			<div style="height: 1000px">a</div>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
