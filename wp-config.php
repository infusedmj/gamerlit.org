<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp331');

/** MySQL database username */
define('DB_USER', 'wp331');

/** MySQL database password */
define('DB_PASSWORD', 'q2O@9p3yS!');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mn0lj33cwzyqvzj9593jjhhdart73vmoh5u3ut2ldz7mtomildwf01dynq0cjmjg');
define('SECURE_AUTH_KEY',  'coitpfzb7tydoigwkagj1x11ohevjnoec9rv9qe61lylzwtdnhrkh8dgtraxr2xg');
define('LOGGED_IN_KEY',    '8mzjt2doxsfibv2cbuzwhvq37vldriam6qrcws0eslambhltwuqud0omw2lxgzxi');
define('NONCE_KEY',        '9i6otgxasyz2iqsta5oarmsiuqydtk0q1pr5fvbn6ngn398xqtzhgydafe61wsax');
define('AUTH_SALT',        'iy2e3t0wg1r2muac0nv3tkvwmpokaahlaoejxjr4ix59djxa3dh0rd3koxqnyc0q');
define('SECURE_AUTH_SALT', 'xvlidr74angtrqnbddpxcvnfsqsu3rwe9npqsalvlgcp9q1oyukqpmiwrxkvuvvu');
define('LOGGED_IN_SALT',   'wn9rhvdgslncdps4nsrdqttf4htgmw8hscwjaw9chasaxkwvwwrj6iyzggo2ngsy');
define('NONCE_SALT',       '6035ln8pwzw2s9vg23jjhu8vqung69ppc6gl1ipwtwqclvly1tpjghkc1z1z1tir');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpgc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
