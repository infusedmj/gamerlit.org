jQuery(document).ready(function() {
  jQuery(window).on("scroll", function() {
    var fromTop = window.scrollY;

    if(fromTop > 300) {
    	jQuery('.site-header-wrap').show();
    } else {
    	jQuery('.site-header-wrap').hide();
    }
    
  });

  jQuery('.menu-main-toggle').click(function() {
  	jQuery('.mobile-menu-wrap').toggle();
  });
});