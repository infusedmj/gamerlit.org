<div id="page-sub-header" class="site-header" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
                    <div class="navbar-transparent">
                        <div class="navbar-wrap">
                            <div class="nav-controls">
                                <div style="float:right">
                                <span class="nav-control">
                                    <img src="<?php echo GAMERLIT_2X . '02-search-ICON.png'?>" />
                                </span>
                                <?php if ( class_exists( 'WooCommerce' ) ) : ?>
                                     <span class="nav-control">
                                        <img src="<?php echo GAMERLIT_2X . '04-gamerlitCart.png'?>" />
                                        <span class="cart-num"><?php echo WC()->cart->cart_contents_count; ?></span>
                                    </span>
                                <?php endif; ?>
                                <span class="nav-control menu-main-toggle">
                                    <img src="<?php echo GAMERLIT_2X . 'menu-icon.png'?>" />
                                </span>
                                </div>
                            </div>

                            <a href="<?php echo esc_url( home_url( '/' )); ?>">
                                <div class="logo-wrap">
                                    <div class="gl-logo"></div>
                                </div>
                                <div class="web-nav">
                                    <?php if ( has_nav_menu( 'pustaka-primary' ) ) : 
                                    $menu_args = array(
                                        'theme_location' => 'pustaka-primary',
                                        'container'      => false
                                    );

                                    if ( class_exists( 'Tokoo_Megamenus_Walker' ) )
                                        $menu_args['walker'] = new Tokoo_Megamenus_Walker;

                                    wp_nav_menu( $menu_args );

                                    else : ?>

                                        <ul class="menu">
                                            <?php wp_list_pages( array( 'depth' => 1,'sort_column' => 'menu_order','title_li' => '', 'include'  => 2 ) ) ?>
                                        </ul>
                                        
                                    <?php endif; ?>
                                </div>
                            </a>




                        </div>
                        <div class="mobile-menu-wrap">
                            <nav class="mobile-menu"></nav>
                           <?php if ( has_nav_menu( 'pustaka-primary' ) ) : 
                            $menu_args = array(
                                'theme_location' => 'pustaka-primary',
                                'container'      => false
                            );

                            if ( class_exists( 'Tokoo_Megamenus_Walker' ) )
                                $menu_args['walker'] = new Tokoo_Megamenus_Walker;

                            wp_nav_menu( $menu_args );

                            else : ?>

                                <ul class="menu">
                                    <?php wp_list_pages( array( 'depth' => 1,'sort_column' => 'menu_order','title_li' => '', 'include'  => 2 ) ) ?>
                                </ul>
                                
                            <?php endif; ?>
                        </div>

                    </div>


                </div>