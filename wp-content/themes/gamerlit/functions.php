<?php


define('GAMERLIT_URL', get_stylesheet_directory_uri() . '/');
define('GAMERLIT_2X', get_stylesheet_directory_uri() . '/assets/2x/');

add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles');

function enqueue_child_theme_styles() {
  wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
  wp_enqueue_style( 'gg-font', 'https://fonts.googleapis.com/css?family=Abel|Khand|Ubuntu' );
  wp_enqueue_script('gamerlit-script', get_stylesheet_directory_uri() . '/script.js', array('jquery'));
}
?>