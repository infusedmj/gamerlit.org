<div id="page-sub-header" class="is-sticky" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
                    <div class="navbar-transparent">
                        <div class="navbar-wrap">
                            <?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
                                <a href="<?php echo esc_url( home_url( '/' )); ?>">
                                    <img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                </a>
                            <?php else : ?>
                                <a href="<?php echo esc_url( home_url( '/' )); ?>">
                                   <div class="gl-logo"></div>
                                </a>
                            <?php endif; ?>

                            <div class="nav-controls">

                            </div>
                        </div>

                    </div>


                </div>